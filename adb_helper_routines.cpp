/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \file
  This file consists of implementation of helper routines used
  in the API.
*/

#include "stdafx.h"
#include "adb_api.h"
#include "adb_api_legacy.h"
#include "adb_helper_routines.h"
#include "adb_interface_enum.h"
//#include <algorithm>
#include <tchar.h>
#include <atlstr.h>
#include <vector>
#include <dbt.h>


#define LABEL_NUMBER	_T("LABELNUMBER")
#define LABEL_LOCPATH	_T("LABELPATH")
#define SHOWFDADBLOG	_T("SHOWFDADBLOG")

std::wstring	LabelNum;
std::wstring	LabelPath;
HANDLE			hEventReset = NULL;
BOOL	bShowFDLog = FALSE;

typedef struct	__tagHubInfo{
	std::wstring		sHubName;
	int					nHubPort;
	USB_CONNECTION_STATUS	usbconStatus;

	std::wstring		sDriveKey;
	std::wstring		sNewDriveKey;

	std::wstring		sLocationPath;
	std::wstring	    sAdbName;

	SP_DEVICE_INTERFACE_DATA interface_data;
	__tagHubInfo()
	{
		nHubPort = 0;
	}
}HUBINFO, *PHUBINFO;

std::map<std::wstring,HUBINFO>		hubInfoMap;

BOOL FindLocationPathMatch(std::vector<std::wstring> vctLocpath, std::wstring loction, std::wstring &sMatchlp);
BOOL FindLocationPathMatchHub(std::vector<std::wstring> vctLocpath, std::wstring loction, HUBINFO &Hubi);

bool to_bool(std::wstring str) {
	//std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	//std::wistringstream is(str);
	//bool b;
	//is >> std::boolalpha >> b;
	return _tcsnccmp(str.c_str(), _T("true"), 4) == 0;
}


void logIt(TCHAR* fmt, ...)
{
	if (!bShowFDLog)
	{
		return;
	}
	va_list args;

	CString sLog;
	va_start(args,fmt);
	sLog.FormatV(fmt, args);
	va_end(args);
	CString sLogpid;
	sLogpid.Format(_T("[Label_%s]:%s"), LabelNum.empty()?_T("0000"):LabelNum.c_str(), sLog);
	OutputDebugString(sLogpid);

}

BOOL GetHubSymblFromLocationPath(std::vector<std::wstring> vctLocpath)
{
	logIt(_T("GetHubSymblFromLocationPath ++ "));
	BOOL bRet = true;
	TCHAR buffer[2048]={0};
	HDEVINFO hDevInfo=SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB_HUB, NULL,NULL, DIGCF_DEVICEINTERFACE |DIGCF_PRESENT);
	if(INVALID_HANDLE_VALUE!=hDevInfo)
	{
		DWORD idx=0;
		SP_DEVICE_INTERFACE_DATA device_interface_data;
		SP_DEVINFO_DATA devinfo_data;
		device_interface_data.cbSize=sizeof(SP_DEVICE_INTERFACE_DATA);
		devinfo_data.cbSize=sizeof(SP_DEVINFO_DATA);

		while(SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &GUID_DEVINTERFACE_USB_HUB, idx++,&device_interface_data))
		{
			PSP_DEVICE_INTERFACE_DETAIL_DATA  pData=(PSP_DEVICE_INTERFACE_DETAIL_DATA )buffer;
			pData->cbSize=sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			if (SetupDiGetDeviceInterfaceDetail(hDevInfo, &device_interface_data, pData, 1000, NULL, &devinfo_data))
			{
				HUBINFO	tmpHubInfo;
				tmpHubInfo.sHubName = pData->DevicePath;
				DWORD dwLen=0;
				if(SetupDiGetDeviceRegistryProperty(hDevInfo, &devinfo_data, SPDRP_LOCATION_PATHS, NULL, (BYTE*)buffer, MAX_PATH, &dwLen))
				{
					if (FindLocationPathMatchHub(vctLocpath, buffer, tmpHubInfo))
					{
						logIt(_T("sHubName = %s"), tmpHubInfo.sHubName.c_str());
						hubInfoMap.insert(std::pair<std::wstring,HUBINFO>(tmpHubInfo.sLocationPath, tmpHubInfo));
					}
				}
			}
			else
			{
				logIt(_T("SetupDiGetDeviceInterfaceDetail = %d"), GetLastError());
			}

		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
		bRet = FALSE;

	return bRet;
}

BOOL GetHubPortInfo(HUBINFO *tmpHubInfo)
{
	BOOL bRet = FALSE;
	logIt(_T("Open %s"), tmpHubInfo->sHubName.c_str());
	HANDLE hHubDev = CreateFile(tmpHubInfo->sHubName.c_str(),
		GENERIC_WRITE,
		FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	if (hHubDev!=INVALID_HANDLE_VALUE)
	{
		DWORD data_len = sizeof(USB_NODE_CONNECTION_INFORMATION_EX)+sizeof(USB_PIPE_INFO)*32;
		UCHAR ConnectInfoBuf[sizeof(USB_NODE_CONNECTION_INFORMATION_EX)+sizeof(USB_PIPE_INFO)*32]={0};
		PUSB_NODE_CONNECTION_INFORMATION_EX conn_info = (PUSB_NODE_CONNECTION_INFORMATION_EX)(ConnectInfoBuf);
		conn_info->ConnectionIndex = tmpHubInfo->nHubPort;

		if(! DeviceIoControl(hHubDev, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, conn_info, data_len, conn_info, data_len, &data_len, 0))
		{
			logIt(_T("IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX error %d"), GetLastError());
		}  
		else
		{
			logIt(_T("device connect status: %d\n"),conn_info->ConnectionStatus);
			tmpHubInfo->usbconStatus = conn_info->ConnectionStatus;
			if (conn_info->ConnectionStatus == DeviceConnected)
			{
				bRet = TRUE;
				//Get Driverkey
				UCHAR   DriverKeyBuf[sizeof(USB_NODE_CONNECTION_DRIVERKEY_NAME) +
					MAXIMUM_USB_STRING_LENGTH];
				ULONG nBytes = sizeof(DriverKeyBuf);
				PUSB_NODE_CONNECTION_DRIVERKEY_NAME key_name = (PUSB_NODE_CONNECTION_DRIVERKEY_NAME)DriverKeyBuf;
				key_name->ConnectionIndex = tmpHubInfo->nHubPort;
				if(! DeviceIoControl(hHubDev, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME, key_name, nBytes, key_name, nBytes, &nBytes, 0))
				{
					logIt(_T("IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME error %d"), GetLastError());
				}
				else
				{
					tmpHubInfo->sDriveKey = tmpHubInfo->sNewDriveKey;
					tmpHubInfo->sNewDriveKey = key_name->DriverKeyName;
					if (tmpHubInfo->sDriveKey != tmpHubInfo->sNewDriveKey)
					{
						tmpHubInfo->sAdbName = _T("");
					}
				}

			}
			else
			{
				tmpHubInfo->sAdbName = _T("");
			}

		}

		CloseHandle(hHubDev);
	}
	else
	{
		hubInfoMap.clear();
	}
	logIt(_T("GetHubPortInfo -- %s"), bRet?_T("TRUE"):_T("FALSE"));
	return bRet;
}

BOOL	CheckAdbNames()
{
	BOOL bRet = TRUE;
	std::map<std::wstring,HUBINFO>::iterator it;
	for(it=hubInfoMap.begin();it!=hubInfoMap.end();++it)
	{
		if (it->second.sAdbName.empty())
		{
			bRet = FALSE;
		}
	}
	logIt(_T("CheckAdbNames -- %s"), bRet?_T("TRUE"):_T("FALSE"));
	return bRet;
}


BOOL GetEnvValue(TCHAR *sName, std::wstring &sValue)
{
#define BUFSIZE 4096
	BOOL bRet = FALSE;
	DWORD dwRet, dwErr;
    LPTSTR pszOldVal; 
	
	if(sName == NULL)
		return bRet;
	
	pszOldVal = (LPTSTR) malloc(BUFSIZE*sizeof(TCHAR));
    if(NULL == pszOldVal)
    {
        logIt(_T("Out of memory\n"));
        return FALSE;
    }
	
	dwRet = GetEnvironmentVariable(sName, pszOldVal, BUFSIZE);

    if(0 == dwRet)
    {
        dwErr = GetLastError();
        if( ERROR_ENVVAR_NOT_FOUND == dwErr )
        {
            logIt(_T("Environment variable %s does not exist.\n"), sName);
        }
    }
	else if(BUFSIZE < dwRet)
    {
        pszOldVal = (LPTSTR) realloc(pszOldVal, dwRet*sizeof(TCHAR));   
        if(NULL == pszOldVal)
        {
            logIt(_T("Out of memory\n"));
            return FALSE;
        }
        dwRet = GetEnvironmentVariable(sName, pszOldVal, dwRet);
        if(!dwRet)
        {
            logIt(_T("GetEnvironmentVariable failed (%d)\n"), GetLastError());
        }
        else 
		{
			sValue = pszOldVal;
			bRet=TRUE;
		}
    }
	else 
	{
			sValue = pszOldVal;
			bRet=TRUE;		
	}
	
	if(pszOldVal!=NULL)
	{
		free(pszOldVal);
		pszOldVal = NULL;
	}
	
	
	return bRet;
} 

BOOL FindLocationPathMatchHub(std::vector<std::wstring> vctLocpath, std::wstring loction, HUBINFO &Hubi)
{
	BOOL bRet = FALSE;

	std::vector<std::wstring>::iterator iter;
	logIt(_T("FindLocationPathMatchHub ++ %s"), loction.c_str());
	for ( iter = vctLocpath.begin(); iter != vctLocpath.end(); iter++ )
	{
		std::wstring sss = *iter;
		std::wstring::size_type found = sss.rfind(_T("#USB("));
		if (found!=std::wstring::npos)
		{
			std::wstring sleft = sss.substr(0, found);
			std::wstring sright = sss.substr(found);
			logIt(_T("hub Location %s, index %s"), sleft.c_str(), sright.c_str());
			if (sleft.compare(loction)==0)
			{
				bRet = TRUE;
				Hubi.sLocationPath = sss;
				if(_stscanf_s(sright.c_str(),_T("#USB(%d)"), &Hubi.nHubPort)!=1)
				{
					logIt(_T("Can not get hub port."));
					bRet = FALSE;
				}
				break;
			}
		}
	}
	logIt(_T("FindLocationPathMatchHub -- %s"), bRet?_T("TRUE"):_T("FALSE"));
	return bRet;
}

BOOL FindLocationPathMatch(std::vector<std::wstring> vctLocpath, std::wstring loction, std::wstring &sMatchlp)
{
	BOOL bRet = FALSE;

	std::vector<std::wstring>::iterator iter;
	for ( iter = vctLocpath.begin(); iter != vctLocpath.end(); iter++ )
	{
		if (loction.find(*iter) == 0)
		{
			sMatchlp = *iter;
			bRet = TRUE;
			break;
		}
	}

	return bRet;
}

int Split(const std::wstring& str, std::vector<std::wstring>& ret_, std::wstring sep)
{
	logIt((TCHAR*)str.c_str());
	if (str.empty())
	{
		return 0;
	}

	std::wstring tmp;
	std::wstring::size_type pos_begin = str.find_first_not_of(sep);
	std::wstring::size_type comma_pos = 0;

	while (pos_begin != std::wstring::npos)
	{
		comma_pos = str.find(sep, pos_begin);
		if (comma_pos != std::wstring::npos)
		{
			tmp = str.substr(pos_begin, comma_pos - pos_begin);
			pos_begin = comma_pos + sep.length();
		}
		else
		{
			tmp = str.substr(pos_begin);
			pos_begin = comma_pos;
		}

		if (!tmp.empty())
		{
			ret_.push_back(tmp);
			tmp = _T("");
		}
	}
	return 0;
}

void GetEnvironmentSettings()
{
	std::wstring temp;
	if (GetEnvValue(LABEL_NUMBER, LabelNum))
	{
		if(hEventReset != NULL)
		{
			CloseHandle(hEventReset);
			hEventReset = NULL;
		}
		std::wstring labelevent(_T("label_fd_reset_adb_"));
		labelevent+=LabelNum;
		SECURITY_ATTRIBUTES sa;
		sa.lpSecurityDescriptor = (PSECURITY_DESCRIPTOR)malloc(SECURITY_DESCRIPTOR_MIN_LENGTH);
		InitializeSecurityDescriptor(sa.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
	// ACL is set as NULL in order to allow all access to the object.
		SetSecurityDescriptorDacl(sa.lpSecurityDescriptor, TRUE, (PACL)NULL, FALSE);
		sa.nLength = sizeof(sa);
		sa.bInheritHandle = TRUE;

		hEventReset = CreateEvent(&sa, TRUE, FALSE, labelevent.c_str());
		if(hEventReset == NULL)
		{
			logIt(_T("Create %s failed: %d"), labelevent.c_str(), GetLastError());
		}
	}
	if (GetEnvValue(LABEL_LOCPATH, LabelPath))
	{

	}
	if (GetEnvValue(SHOWFDADBLOG, temp))
	{
		bShowFDLog = to_bool(temp);
	}

}

bool GetSDKComplientParam(AdbOpenAccessType access_type,
                          AdbOpenSharingMode sharing_mode,
                          ULONG* desired_access,
                          ULONG* desired_sharing) {
  if (NULL != desired_access) {
    switch (access_type) {
      case AdbOpenAccessTypeReadWrite:
        *desired_access = GENERIC_READ | GENERIC_WRITE;
        break;

      case AdbOpenAccessTypeRead:
        *desired_access = GENERIC_READ;
        break;

      case AdbOpenAccessTypeWrite:
        *desired_access = GENERIC_WRITE;
        break;

      case AdbOpenAccessTypeQueryInfo:
        *desired_access = FILE_READ_ATTRIBUTES | FILE_READ_EA;
        break;

      default:
        SetLastError(ERROR_INVALID_ACCESS);
        return false;
    }
  }

  if (NULL != desired_sharing) {
    switch (sharing_mode) {
      case AdbOpenSharingModeReadWrite:
        *desired_sharing = FILE_SHARE_READ | FILE_SHARE_WRITE;
        break;

      case AdbOpenSharingModeRead:
        *desired_sharing = FILE_SHARE_READ;
        break;

      case AdbOpenSharingModeWrite:
        *desired_sharing = FILE_SHARE_WRITE;
        break;

      case AdbOpenSharingModeExclusive:
        *desired_sharing = 0;
        break;

      default:
        SetLastError(ERROR_INVALID_PARAMETER);
        return false;
    }
  }

  return true;
}

bool EnumerateDeviceInterfaces(HDEVINFO hardware_dev_info,
                               GUID class_id,
                               bool exclude_removed,
                               bool active_only,
                               AdbEnumInterfaceArray* interfaces) {
  AdbEnumInterfaceArray tmp;
  bool ret = false;

  std::vector<std::wstring> vctLocpathes;
  Split(LabelPath, vctLocpathes, _T(";"));
  
  if(WaitForSingleObject(hEventReset, 0) == WAIT_OBJECT_0)
  {
	  logIt(_T("Recieve event, and return "));
	  interfaces->swap(tmp);
	  return true;
  }
  
  if (!vctLocpathes.empty())
  {
	  if(hubInfoMap.empty())
	  {
		  GetHubSymblFromLocationPath(vctLocpathes);
	  }

	  if(CheckAdbNames())
	  {
		  std::map<std::wstring,HUBINFO>::iterator it;
		  for(it=hubInfoMap.begin();it!=hubInfoMap.end();++it)
		  {
			  if(GetHubPortInfo(&it->second)&&it->second.sAdbName.size()>0)
			  {
				  try {
					  // Add new entry to the array
					  logIt(_T("Add new to array %s"), it->second.sAdbName.c_str());
					  tmp.push_back(AdbInstanceEnumEntry(it->second.sAdbName.c_str(),
						  it->second.interface_data.InterfaceClassGuid,
						  it->second.interface_data.Flags));
				  } catch (... ) {
					  SetLastError(ERROR_OUTOFMEMORY);
					  break;
				  }

			  }
		  }
		  interfaces->swap(tmp);
		  return true;
	  }
	  else
	  {
		  std::map<std::wstring,HUBINFO>::iterator it;
		  for(it=hubInfoMap.begin();it!=hubInfoMap.end();++it)
		  {
			  GetHubPortInfo(&it->second);
		  }
	  }
   }


  BOOL bFind = FALSE;

  // Enumerate interfaces on this device
  for (ULONG index = 0; !bFind; index++) {
    SP_DEVICE_INTERFACE_DATA interface_data;
    interface_data.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

    // SetupDiEnumDeviceInterfaces() returns information about device
    // interfaces exposed by one or more devices defined by our interface
    // class. Each call returns information about one interface. The routine
    // can be called repeatedly to get information about several interfaces
    // exposed by one or more devices.
    if (SetupDiEnumDeviceInterfaces(hardware_dev_info,
                                    0, 
                                    &class_id,
                                    index,
                                    &interface_data)) {
		
      // Satisfy "exclude removed" and "active only" filters.
      if ((!exclude_removed || (0 == (interface_data.Flags & SPINT_REMOVED))) &&
          (!active_only || (interface_data.Flags & SPINT_ACTIVE))) {
        std::wstring dev_name;
		std::wstring sCurLocationpath;

		if(!vctLocpathes.empty())
		{
			ULONG required_len = 0;
			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			  // First query for the structure size. At this point we expect this call
			  // to fail with ERROR_INSUFFICIENT_BUFFER error code.
			if (SetupDiGetDeviceInterfaceDetail(hardware_dev_info,
												  &interface_data,
												  NULL,
												  0,
												  &required_len,
												  &devInfoData))
			{
				logIt(_T("SetupDiGetDeviceInterfaceDetail error = %d"), GetLastError());
				break;
			}
			
			 if (ERROR_INSUFFICIENT_BUFFER != GetLastError())
			 {
				 logIt(_T("SetupDiGetDeviceInterfaceDetail lasterror = %d"), GetLastError());
				 break;
			 }

			DWORD sz = 0;
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			
			
			sz = 0;
			
/*
			if (SetupDiGetDeviceRegistryProperty(
				hardware_dev_info,
				&devInfoData,
				SPDRP_LOCATION_PATHS,
				&type, b, 2048, &sz))
			{
				TCHAR *instanceid = (TCHAR *)b;
				//DWORD dwoffset = 0;
				while (instanceid != NULL && _tcslen(instanceid)>0)
				{
					logIt(_T("Get location path:%s"),instanceid);
					//bFind = std::find(vctLocpathes.begin(), vctLocpathes.end(), instanceid)!=vctLocpathes.end();
					//dwoffset += _tcslen(instanceid) + 1;
					//instanceid = (TCHAR *)b + dwoffset;
					bFind = FindLocationPathMatch(vctLocpathes, instanceid);
					if(bFind) break;
					instanceid += _tcslen(instanceid) + 1;
				}
			}
			else
			{
				logIt(_T("Can not SetupDiGetDeviceRegistryProperty %d"), GetLastError());
			}
			if (!bFind) continue;
//*/			
///*			
			if (SetupDiGetDeviceProperty(hardware_dev_info, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
			{
				if (type == DEVPROP_TYPE_STRING_LIST)
				{
					TCHAR *instanceid = (TCHAR *)b;
					//DWORD dwoffset = 0;
					while (instanceid != NULL && _tcslen(instanceid)>0)
					{
						logIt(_T("Get location path:%s"),instanceid);
						
						//bFind = std::find(vctLocpathes.begin(), vctLocpathes.end(), instanceid)!=vctLocpathes.end();
						bFind = FindLocationPathMatch(vctLocpathes, instanceid, sCurLocationpath);
						if(bFind) 
						{
							break;
						}
						instanceid += _tcslen(instanceid) + 1;						
					}
				}
			}
			else
			{
				logIt(_T("Can not SetupDiGetDeviceProperty %d"), GetLastError());
			}
			if (!bFind) continue;
//*/
		}


        if (GetUsbDeviceName(hardware_dev_info, &interface_data, &dev_name)) {
          try {
            // Add new entry to the array
			std::map<std::wstring,HUBINFO>::iterator iit = hubInfoMap.find(sCurLocationpath);
			if (iit!=hubInfoMap.end())
			{
				iit->second.sAdbName = dev_name;
				memcpy(&iit->second.interface_data, &interface_data, sizeof(SP_DEVICE_INTERFACE_DATA));
			}
			logIt(_T("GetUsbDeviceName add array %s"),dev_name.c_str());
            tmp.push_back(AdbInstanceEnumEntry(dev_name.c_str(),
                                               interface_data.InterfaceClassGuid,
                                               interface_data.Flags));
          } catch (... ) {
            SetLastError(ERROR_OUTOFMEMORY);
            break;
          }
        } else {
          // Something went wrong in getting device name
          break;
        }
      }
    } else {
      if (ERROR_NO_MORE_ITEMS == GetLastError()) {
        // There are no more items in the list. Enum is completed.
        ret = true;
        break;
      } else {
        // Something went wrong in SDK enum
        break;
      }
    }
  }

  // On success, swap temp array with the returning one
  if (ret)
    interfaces->swap(tmp);

  return ret;
}

bool EnumerateDeviceInterfaces(GUID class_id,
                               ULONG flags,
                               bool exclude_removed,
                               bool active_only,
                               AdbEnumInterfaceArray* interfaces) {
  // Open a handle to the plug and play dev node.
  // SetupDiGetClassDevs() returns a device information set that
  // contains info on all installed devices of a specified class.
  logIt(_T("EnumerateDeviceInterfaces ++ "));
  HDEVINFO hardware_dev_info =
    SetupDiGetClassDevs(&class_id, NULL, NULL, flags);

  bool ret = false;

  if (INVALID_HANDLE_VALUE != hardware_dev_info) {
    // Do the enum
    ret = EnumerateDeviceInterfaces(hardware_dev_info,
                                    class_id,
                                    exclude_removed,
                                    active_only,
                                    interfaces);

    // Preserve last error accross hardware_dev_info destruction
    ULONG error_to_report = ret ? NO_ERROR : GetLastError();

    SetupDiDestroyDeviceInfoList(hardware_dev_info);

    if (NO_ERROR != error_to_report)
      SetLastError(error_to_report);
  }

  return ret;
}

bool GetUsbDeviceDetails(
    HDEVINFO hardware_dev_info,
    PSP_DEVICE_INTERFACE_DATA dev_info_data,
    PSP_DEVICE_INTERFACE_DETAIL_DATA* dev_info_detail_data) {
  ULONG required_len = 0;

  // First query for the structure size. At this point we expect this call
  // to fail with ERROR_INSUFFICIENT_BUFFER error code.
  if (SetupDiGetDeviceInterfaceDetail(hardware_dev_info,
                                      dev_info_data,
                                      NULL,
                                      0,
                                      &required_len,
                                      NULL)) {
    return false;
  }

  if (ERROR_INSUFFICIENT_BUFFER != GetLastError())
    return false;

  // Allocate buffer for the structure
  PSP_DEVICE_INTERFACE_DETAIL_DATA buffer =
    reinterpret_cast<PSP_DEVICE_INTERFACE_DETAIL_DATA>(malloc(required_len));

  if (NULL == buffer) {
    SetLastError(ERROR_OUTOFMEMORY);
    return false;
  }

  buffer->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

  // Retrieve the information from Plug and Play.
  if (SetupDiGetDeviceInterfaceDetail(hardware_dev_info,
                                      dev_info_data,
                                      buffer,
                                      required_len,
                                      &required_len,
                                      NULL)) {
    *dev_info_detail_data = buffer;
    return true;
  } else {
    // Free the buffer if this call failed
    free(buffer);

    return false;
  }
}

bool GetUsbDeviceName(HDEVINFO hardware_dev_info,
                      PSP_DEVICE_INTERFACE_DATA dev_info_data,
                      std::wstring* name) {
  PSP_DEVICE_INTERFACE_DETAIL_DATA func_class_dev_data = NULL;
  if (!GetUsbDeviceDetails(hardware_dev_info,
                           dev_info_data,
                           &func_class_dev_data)) {
    return false;
  }

  try {
    *name = func_class_dev_data->DevicePath;
  } catch (...) {
    SetLastError(ERROR_OUTOFMEMORY);
  }

  free(func_class_dev_data);

  return !name->empty();
}

bool IsLegacyInterface(const wchar_t* interface_name) {
  // Open USB device for this intefface
  HANDLE usb_device_handle = CreateFile(interface_name,
                                        GENERIC_READ | GENERIC_WRITE,
                                        FILE_SHARE_READ | FILE_SHARE_WRITE,
                                        NULL,
                                        OPEN_EXISTING,
                                        0,
                                        NULL);
  if (INVALID_HANDLE_VALUE == usb_device_handle)
    return NULL;

  // Try to issue ADB_IOCTL_GET_USB_DEVICE_DESCRIPTOR IOCTL that is supported
  // by the legacy driver, but is not implemented in the WinUsb driver.
  DWORD ret_bytes = 0;
  USB_DEVICE_DESCRIPTOR descriptor;
  BOOL ret = DeviceIoControl(usb_device_handle,
                             ADB_IOCTL_GET_USB_DEVICE_DESCRIPTOR,
                             NULL, 0,
                             &descriptor,
                             sizeof(descriptor),
                             &ret_bytes,
                             NULL);
  ::CloseHandle(usb_device_handle);

  // If IOCTL succeeded we've got legacy driver underneath.
  return ret ? true : false;
}
