@echo off

set DDKROOT=D:\WinDDK\7600.16385.1
set BASEDIR=%DDKROOT%
set     _DRIVERNAME=%1
echo .                                                   
echo +++++ Starting the '%2' build of the %_DRIVERNAME% driver ( BASEDIR = %BASEDIR% ) +++++
echo .     
echo %DDKROOT%                                   

if "%2"=="fre" goto CHK_EXIT
if "%2"=="FRE" goto CHK_EXIT
if "%3"=="W2K" goto CHK_W2K
if "%3"=="w2k" goto CHK_W2K
if "%3"=="WXP" goto CHK_WXP
if "%3"=="wxp" goto CHK_WXP
set OUTPATH=objchk_wnet_x86
goto CHK_EXIT
:CHK_W2K
set OUTPATH=objchk_w2k_x86
goto CHK_EXIT
:CHK_WXP
set OUTPATH="bjchk_wxp_x86
:CHK_EXIT

if "%2"=="chk" goto FRE_EXIT
if "%2"=="CHK" goto FRE_EXIT
if "%3"=="W2K" goto FRE_W2K
if "%3"=="w2k" goto FRE_W2K
if "%3"=="WXP" goto FRE_WXP
if "%3"=="wxp" goto FRE_WXP
set OUTPATH=objfre_wnet_x86
goto FRE_EXIT
:FRE_W2K
set OUTPATH=objfre_w2k_x86
goto FRE_EXIT
:FRE_WXP
set OUTPATH=objfre_wxp_x86
:FRE_EXIT   

del     %OUTPATH%\*.mac   
del     %OUTPATH%\I386\*.obj  >nul    
del     %OUTPATH%\I386\*.res  >nul    
del     %OUTPATH%\I386\*.sys  >nul     
del     %OUTPATH%\I386\*.map  >nul     
rem del     %OUTPATH%\I386\*.asm  >nul     
del     %OUTPATH%\I386\*.sym  >nul     
rem del     %OUTPATH%\I386\*.sbr  >nul
del     %OUTPATH%\I386\*.pdb       

del     %BASEDIR%\build.dat >nul

rem %BASEDIR%\bin\prchdir > %BASEDIR%\@temp.bat
pushd .
call    %BASEDIR%\bin\setenv.bat %BASEDIR% %2 %3
popd
rem call    %BASEDIR%\@temp.bat
rem del     %BASEDIR%\@temp.bat

set     DATETIME="Unknown"
datetime > %BASEDIR%\@temp.bat
call    %BASEDIR%\@temp.bat
del     %BASEDIR%\@temp.bat
set     DATETIME

set     SZDRIVERNAME="%_DRIVERNAME%"  
set     USDRIVERNAME=L"%_DRIVERNAME%"  

if "%2"=="fre" goto free
if "%2"=="FRE" goto free
echo Setting additional defines for 'checked' build  
set     C_DEFINES= %C_DEFINES% -DDBG -DSZDRVNAME="""%_DRIVERNAME%""" -DUSDRVNAME=L"""%_DRIVERNAME%""" 
:free

if "%2"=="chk" goto checked
if "%2"=="CHK" goto checked
echo Setting additional defines for 'free' build
set     C_DEFINES= %C_DEFINES%       -DSZDRVNAME="""%_DRIVERNAME%""" -DUSDRVNAME=L"""%_DRIVERNAME%"""
:checked

set     RCOPTIONS= %RCOPTIONS% -DVER_BUILDDATE_STR="""%DATETIME%"""

set     TARGETPATH=.
rem del     %TARGETPATH%\%OUTPATH%\I386\%1.sys

rem %BASEDIR%\bin\build.exe   -j %1 -w -f 
build.exe   -j %1 -w -f -b

rem del     *.bsc
rem #### shuffle around the *.bsc 
rem copy    %TARGETPATH%\%OUTPATH%\I386\%1.bsc
rem del     %TARGETPATH%\%OUTPATH%\I386\%1.bsc
rem exit

goto exit
:usage
echo usage _build.bat Log_Filename fre/chk W2K/WXP/WNET [build_options]
echo eg _build.bat virtualdisk chk W2K -cef 
:exit